library(randomizeR)
library(ggdist)

setwd("/rwthfs/rz/cluster/home/fo152706/Tabellen Dateien")
file.create("OLS_Endpunkte2_CHEN_mean_error.csv")
file.create("OLS_Endpunkte2_CHEN_proz.csv")

N<-list(10,20,50,100)
Endpunkte<-2
Correlation<- list(0,0.2,0.4,0.6,0.8,1)

eta_10=list(0.02024,0.1012,0.2024)
eta_20=list(0.01325,0.06625,0.1325)
eta_50=list(0.00809,0.04045,0.0809)
eta_100=list(0.0056,0.028,0.056)
eta=list(eta_10,eta_20,eta_50,eta_100)











#unterschiedliche Randomisierunssequenzen
for(p in seq(1,6,1)){
  
  mittlererFehler_t=matrix(0,4,3)
  mittlererFehler_gen=matrix(0,4,3)
  
  Proz_5_t=matrix(0,4,3)
  Proz_5_gen=matrix(0,4,3)
  
  
  nbetr_t=matrix(0,4,3)
  nbetr_gen=matrix(0,4,3)
  
  for (w in seq(1,4,1)){
    for( z in seq(1,3,1)){
      
      rho=Correlation[[p]]
      
      Rand<- chenPar(N[[w]],2,0.67,groups = LETTERS[1:2])
      myPar<- genSeq(Rand,10000)
      Rand_list<- getRandList(myPar)
      for (j in seq(1,dim(Rand_list)[1],1))
        for (i in seq(1,dim(Rand_list)[2],1))
          if ( Rand_list[j,i]=="A"){
            Rand_list[j,i]=1}else{
              Rand_list[j,i]=0}
      
      
      
      
      # Definiton der Anzahl an Patienten in der Kontroll- und Treatmentgruppe
      
      n_E<-numeric(0)
      n_C<-numeric(0)
      
      for (i in seq(1,dim(Rand_list)[1],1)){
        sum_E=0
        for(j in seq(1,dim(Rand_list)[2],1)){
          sum_E=sum_E+strtoi(Rand_list[i,j])}
        n_E=c(n_E,sum_E)
        n_C=c(n_C,N[[w]]-sum_E)}
      
      
      
      
      
      #Allocation Bias
      
      N_E<-function(i,T){
        a<-0
        if(i>0){
          for (j in seq(1,i,1))
            if( T[j]=="1"){
              a<-a+1}
          return(a)}
        else{
          return(0)}}
      
      N_C<-function(i,T){
        a<-0
        if(i>0){
          for (j in seq(1,i,1))
            if( T[j]=="0"){
              a<-a+1}
          return(a)}
        else{
          return(0)}}
      
      
      tau<-function(eta,i,T){
        if(i>1){
          return (eta*(sign(N_E(i-1,T)-N_C(i-1,T))))}
        if(i==1){
          return(0)}}
      
      #1. Determine Allocation Bias Effect
      
      eta_1<-eta[[w]][[z]]
      eta_2<-eta[[w]][[z]]
      
      #1. Berechne Location Shift
      
      tau_1_E <- numeric(0)
      tau_1_C<-numeric(0)
      
      tau_2_E <- numeric(0)
      tau_2_C<-numeric(0)
      
      
      for (i in seq(1,dim(Rand_list)[1],1)){
        sum_1_E=0
        sum_1_C=0
        sum_2_E=0
        sum_2_C=0
        for(j in seq(1,dim(Rand_list)[2],1)){
          sum_1_E=sum_1_E+tau(eta_1,j,Rand_list[i,1:dim(Rand_list)[2]])*strtoi(Rand_list[i,j])
          sum_1_C=sum_1_C+tau(eta_1,j,Rand_list[i,1:dim(Rand_list)[2]])*(1-strtoi(Rand_list[i,j]))
          
          sum_2_E=sum_2_E+tau(eta_2,j,Rand_list[i,1:dim(Rand_list)[2]])*strtoi(Rand_list[i,j])
          sum_2_C=sum_2_C+tau(eta_2,j,Rand_list[i,1:dim(Rand_list)[2]])*(1-strtoi(Rand_list[i,j]))}
        
        tau_2_E<-c(tau_2_E,1/n_E[i]*sum_2_E)
        tau_2_C<-c(tau_2_C,1/n_C[i]*sum_2_C)
        
        tau_1_E<-c(tau_1_E,1/n_E[i]*sum_1_E)
        tau_1_C<-c(tau_1_C,1/n_C[i]*sum_1_C)}
      
      
      
      
      location<-numeric(0)
      
      
      for (i in seq(1,dim(Rand_list)[1],1)){
        location<-c(location, sqrt(n_E[i]*n_C[i]/N[[w]])*1/sqrt(Endpunkte*(Endpunkte*rho-rho+1))*((tau_1_C[i]-tau_1_E[i])+(tau_2_C[i]-tau_2_E[i])))}
      
      scale<-rep(1, times=dim(Rand_list)[1])
      
      
      alpha=0.05
      
      df<-numeric(0)
      
      for (i in seq(1,dim(Rand_list)[1],1)){
        df<-c(df,1/2*(N[[w]]-2)*(1+1/(Endpunkte)^2))}
      
      
      
      quantiles_gen<-numeric(0)
      
      for (i in seq(1,dim(Rand_list)[1],1)){
        quantiles_gen<-c(quantiles_gen,qstudent_t(1-alpha/2,df[i],location[i],scale[i]))}
      
      
      
      quantiles_t<-numeric(0)
      
      for (i in seq(1,dim(Rand_list)[1],1)){
        quantiles_t<-c(quantiles_t,qt(1-alpha/2,df[i]))}
      
      
      
      error_gen<-numeric(0)
      error_t<-numeric(0)
      
      for (i in seq(1,dim(Rand_list)[1],1)){
        error_gen<-c(error_gen,pstudent_t(-quantiles_gen[i],df[i],location[i],scale[i])+1-pstudent_t(quantiles_gen[i],df[i],location[i],scale[i]))
        error_t<-c(error_t, pstudent_t(-quantiles_t[i],df[i],location[i],scale[i])+1-pstudent_t(quantiles_t[i],df[i],location[i],scale[i]))}
      
      error_m_t<-0
      j=0
      for (i in seq(1,dim(Rand_list)[1],1)){
        if(error_t[i]=="NaN"){
          error_m_t=error_m_t+0
          j=j+1}
        else{
          
          error_m_t=error_m_t+error_t[i]}}
      
      
      error_m_t=error_m_t/(dim(Rand_list)[1]-j)
      mittlererFehler_t[w,z]=error_m_t
      nbetr_t[w,z]=j
      
      
      error_m_gen<-0
      j=0
      for (i in seq(1,dim(Rand_list)[1],1)){
        if(error_gen[i]=="NaN"){
          error_m_gen=error_m_gen+0
          j=j+1}
        else{
          
          error_m_gen=error_m_gen+error_gen[i]}}
      
      
      error_m_gen=error_m_gen/(dim(Rand_list)[1]-j)
      mittlererFehler_gen[w,z]=error_m_gen
      nbetr_gen[w,z]=j
      
      underfive=0
      abovefive=0
      notanumb=0
      for (i in seq(1,dim(Rand_list)[1],1)){
        if(error_t[i]=='NaN'){
          notanumb=notanumb+1}
        else if(error_t[i]<=0.05){
          underfive=underfive+1}
        else {
          abovefive=abovefive+1}}
      
      
      proz=underfive/(dim(Rand_list)[1]-notanumb)*100
      Proz_5_t[w,z]=proz
      
      
      underfive=0
      abovefive=0
      notanumb=0
      for (i in seq(1,dim(Rand_list)[1],1)){
        if(error_gen[i]=='NaN'){
          notanumb=notanumb+1}
        else if(error_gen[i]<=0.05){
          underfive=underfive+1}
        else {
          abovefive=abovefive+1}}
      
      
      proz=underfive/(dim(Rand_list)[1]-notanumb)*100
      Proz_5_gen[w,z]=proz}}
  
  print(rho)
  
  
  print("quantile t-distributed")
  print(mittlererFehler_t)
  print(nbetr_t)
  print(Proz_5_t)
  
  print("generalized quantile t-distributed")
  
  print(mittlererFehler_gen)
  write.table(mittlererFehler_gen, file="OLS_Endpunkte2_CHEN_mean_error.csv", append=TRUE ,row.names=FALSE,col.names = FALSE)
  print(nbetr_gen)
  print(Proz_5_gen)
  write.table(Proz_5_gen, file="OLS_Endpunkte2_CHEN_proz.csv", append=TRUE ,row.names=FALSE,col.names = FALSE)}








