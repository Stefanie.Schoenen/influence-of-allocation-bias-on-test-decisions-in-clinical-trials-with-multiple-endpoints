#Simulation 2 Endpoints; Standard normal distribution; Chens urn design 

library(randomizeR)
N<-list(10,20,50,100)

eta_10=list(0.02024,0.1012,0.2024)
eta_20=list(0.01325,0.06625,0.1325)
eta_50=list(0.00809,0.04045,0.0809)
eta_100=list(0.0056,0.028,0.056)
eta=list(eta_10,eta_20,eta_50,eta_100)

Fehler<-numeric(0)

setwd("/rwthfs/rz/cluster/home/fo152706/Tabellen Dateien")

file.create("AllorNone_Endpunkte2_CHEN_mean_error.csv")
file.create("AllorNone_Endpunkte2_CHEN_proz.csv")
file.create("AllorNone_Endpunkte2_CHEN_error.csv")

mittlererFehler=matrix(0,4,3)
Proz_5=matrix(0,4,3)
Proz_55=matrix(0,4,3)
nbetr=matrix(0,4,3)







#Build the randomisation sequence of CHEN(2,0.67)

for (w in seq(1,4,1)){
  for( z in seq(1,3,1)){
    
    Rand<- chenPar(N[[w]],2,0.67,groups = LETTERS[1:2])
    myPar<- genSeq(Rand,10000)
    Rand_list<- getRandList(myPar)
    for (j in seq(1,dim(Rand_list)[1],1))
      for (i in seq(1,dim(Rand_list)[2],1))
        if ( Rand_list[j,i]=="A"){
          Rand_list[j,i]=1}else{
            Rand_list[j,i]=0}
    
    
    # Definition of the number of patients in the treatment and control group
    
    n_E<-numeric(0)
    n_C<-numeric(0)
    
    for (i in seq(1,dim(Rand_list)[1],1)){
      sum_E=0
      for(j in seq(1,dim(Rand_list)[2],1)){
        sum_E=sum_E+strtoi(Rand_list[i,j])}
      n_E=c(n_E,sum_E)
      n_C=c(n_C,N[[w]]-sum_E)}
    
    
    
    # Define the standard normal distributed variables
    x_1=rep(0, times=N[[w]])
    x_2=rep(0, times=N[[w]])
    
    
    mü_1<-x_1
    mü_2<-x_2
    
    
    mü_1_E <- numeric(0)
    mü_1_C<-numeric(0)
    
    mü_2_E <- numeric(0)
    mü_2_C<-numeric(0)
    
    
    
    
    for (i in seq(1,dim(Rand_list)[1],1)){
      sum_1_E=0
      sum_1_C=0
      
      sum_2_E=0
      sum_2_C=0
      
      
      for(j in seq(1,dim(Rand_list)[2],1)){
        sum_1_E=sum_1_E+mü_1[j]*strtoi(Rand_list[i,j])
        sum_1_C=sum_1_C+mü_1[j]*(1-strtoi(Rand_list[i,j]))
        
        sum_2_E=sum_2_E+mü_2[j]*strtoi(Rand_list[i,j])
        sum_2_C=sum_2_C+mü_2[j]*(1-strtoi(Rand_list[i,j]))}
      
      mü_2_E<-c(mü_2_E,1/n_E[i]*sum_2_E)
      mü_2_C<-c(mü_2_C,1/n_C[i]*sum_2_C)
      
      mü_1_E<-c(mü_1_E,1/n_E[i]*sum_1_E)
      mü_1_C<-c(mü_1_C,1/n_C[i]*sum_1_C)}
    
    #Allocation Bias policy
    
    N_E<-function(i,T){
      a<-0
      if(i>0){
        for (j in seq(1,i,1))
          if( T[j]=="1"){
            a<-a+1}
        return(a)}
      else{
        return(0)}}
    
    N_C<-function(i,T){
      a<-0
      if(i>0){
        for (j in seq(1,i,1))
          if( T[j]=="0"){
            a<-a+1}
        return(a)}
      else{
        return(0)}}
    
    
    tau<-function(eta,i,T){
      if(i>1){
        return (eta*(sign(N_E(i-1,T)-N_C(i-1,T))))}
      if(i==1){
        return(0)}}
    
    #Define the error term of the linear model which also considers the allocation bias
    
    
    sigma_1=1
    sigma_2=1
    
    
    epsilon_1<-rnorm(N[[w]],mean=0,sd=sigma_1)
    epsilon_2<-rnorm(N[[w]],mean=0,sd=sigma_2)
    
    
    eta_1<-eta[[w]][[z]]
    eta_2<-eta[[w]][[z]]
    
    #Calculate the parameter of the doubly non-central t-distribution
    #Calculate lambda
    
    tau_1_E <- numeric(0)
    tau_1_C<-numeric(0)
    
    tau_2_E <- numeric(0)
    tau_2_C<-numeric(0)
    
    
    for (i in seq(1,dim(Rand_list)[1],1)){
      sum_1_E=0
      sum_1_C=0
      sum_2_E=0
      sum_2_C=0
      for(j in seq(1,dim(Rand_list)[2],1)){
        sum_1_E=sum_1_E+tau(eta_1,j,Rand_list[i,1:dim(Rand_list)[2]])*strtoi(Rand_list[i,j])
        sum_1_C=sum_1_C+tau(eta_1,j,Rand_list[i,1:dim(Rand_list)[2]])*(1-strtoi(Rand_list[i,j]))
        
        sum_2_E=sum_2_E+tau(eta_2,j,Rand_list[i,1:dim(Rand_list)[2]])*strtoi(Rand_list[i,j])
        sum_2_C=sum_2_C+tau(eta_2,j,Rand_list[i,1:dim(Rand_list)[2]])*(1-strtoi(Rand_list[i,j]))}
      
      tau_2_E<-c(tau_2_E,1/n_E[i]*sum_2_E)
      tau_2_C<-c(tau_2_C,1/n_C[i]*sum_2_C)
      
      tau_1_E<-c(tau_1_E,1/n_E[i]*sum_1_E)
      tau_1_C<-c(tau_1_C,1/n_C[i]*sum_1_C)}
    
    
    
    sum_tau_1<-numeric(0)
    sum_tau_2<-numeric(0)
    
    
    for (i in seq(1,dim(Rand_list)[1],1)){
      sum_1=0
      sum_2=0
      for(j in seq(1,dim(Rand_list)[2],1)){
        sum_1=sum_1+tau(eta_1,j,Rand_list[i,1:dim(Rand_list)[2]])^2
        sum_2=sum_2+tau(eta_2,j,Rand_list[i,1:dim(Rand_list)[2]])^2}
      sum_tau_1=c(sum_tau_1,sum_1)
      sum_tau_2=c(sum_tau_2,sum_2)}
    
    
    
    
    lambda_1<-numeric(0)
    lambda_2<-numeric(0)
    
    for (i in seq(1,dim(Rand_list)[1],1)){
      
      lambda_1=c(lambda_1,1/sigma_1*(sum_tau_1[i]-n_E[i]*tau_1_E[i]^2-n_C[i]*tau_1_C[i]^2))
      lambda_2=c(lambda_2,1/sigma_2*(sum_tau_2[i]-n_E[i]*tau_2_E[i]^2-n_C[i]*tau_2_C[i]^2))}
    
    
    # Calculate delta
    
    delta_1 <- numeric(0)
    delta_2 <- numeric(0)
    
    for (i in seq(1,dim(Rand_list)[1],1)){
      delta_1=c(delta_1,1/sigma_1*sqrt((n_E[i]*n_C[i])/N[[w]])*(mü_1_E[i]-mü_1_C[i]+tau_1_E[i]-tau_1_C[i]))
      delta_2=c(delta_2,1/sigma_2*sqrt((n_E[i]*n_C[i])/N[[w]])*(mü_2_E[i]-mü_2_C[i]+tau_2_E[i]-tau_2_C[i]))}
    
    
    
    #Calculate the type I error probability under misspecification for the All-or-None method
    
    alpha=0.05
    gamma=qt(alpha/2,df=N[[w]]-2)
    
    
    error_1 <- numeric(0)
    error_2 <- numeric(0)
    
    
    for (i in seq(1,dim(Rand_list)[1],1)){
      error_1=c(error_1,doublyT(gamma, N[[w]]-2, delta_1[i], lambda_1[i],ub=100)+doublyT(gamma, N[[w]]-2, -delta_1[i], lambda_1[i],ub=100))
      error_2=c(error_2,doublyT(gamma, N[[w]]-2, delta_2[i], lambda_2[i],ub=100)+doublyT(gamma, N[[w]]-2, -delta_2[i], lambda_2[i],ub=100))}
    
    
    
   
    error<-numeric(0)
    
    
    
    for (i in seq(1,dim(Rand_list)[1],1)){
      error=c(error,max(error_1[i],error_2[i]))}
    
  
      
    Fehler=c(Fehler,error)
    
    #Calculate the type I error probability mean under misspecification above 10000 randomisation sequences 
    
    error_mittel=0
    j=0
    for (i in seq(1,dim(Rand_list)[1],1)){
      if(error[i]=="NaN"){
        error_mittel=error_mittel+0
        j=j+1}
      else{
        
        error_mittel=error_mittel+error[i]}}
    
    
    error_mittel=error_mittel/(dim(Rand_list)[1]-j)
    mittlererFehler[w,z]=error_mittel
    nbetr[w,z]=j
    
    #Calculate the number relative proportion of type I error probability under misspecification <0.05
    
    
    underfive=0
    abovefive=0
    notanumb=0
    for (i in seq(1,dim(Rand_list)[1],1)){
      if(error[i]=='NaN'){
        notanumb=notanumb+1}
      else if(error[i]<=0.05){
        underfive=underfive+1}
      else {
        abovefive=abovefive+1}}
    
    
    proz=underfive/(dim(Rand_list)[1]-notanumb)*100
    Proz_5[w,z]=proz
    
    
    underfive_1=0
    abovefive_1=0
    notanumb_1=0
    for (i in seq(1,dim(Rand_list)[1],1)){
      if(error[i]=='NaN'){
        notanumb_1=notanumb_1+1}
      else if(error[i]<=0.055){
        underfive_1=underfive_1+1}
      else {
        abovefive_1=abovefive_1+1}}
    
    
    proz_1=underfive_1/(dim(Rand_list)[1]-notanumb_1)*100
    Proz_55[w,z]=proz_1}}

print(mittlererFehler)
write.table(mittlererFehler, file="AllorNone_Endpunkte2_CHEN_mean_error.csv", append=TRUE ,row.names=FALSE,col.names = FALSE)
print(Proz_5)
write.table(Proz_5, file="AllorNone_Endpunkte2_CHEN_proz.csv", append=TRUE ,row.names=FALSE,col.names = FALSE)
print(nbetr)
write.csv2(Fehler, "AllorNone_Endpunkte2_CHEN_error.csv")