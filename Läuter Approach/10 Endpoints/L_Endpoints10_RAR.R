library(randomizeR)

biasSeqfun<- function(rs,eta){
  stopifnot(length(rs) %% 2 == 0)
  rs.corrected <- rs[-length(rs)]
  N_E<-cumsum(rs.corrected)
  
  N_C<-numeric(0)
  a<-0
  for (j in seq(1,length(rs.corrected),1)){
    
    if( rs.corrected[j]=="0"){
      a<-a+1
    }
    N_C<-c(N_C,a)}
  out<-c(0)
  for (j in seq(1,length(rs.corrected),1)){
    out=c(out,eta*(sign(N_E[j]-N_C[j])))}
  return(out)
}




samplesizes<-function(n,Seq){
  N_E<-numeric(0)
  N_C<-numeric(0)
  Seq<-apply(Seq,2, as.numeric)
  for (j in 1:nrow(Seq)){
    a<-sum(Seq[j,])
    N_E<-c(N_E,a)
    N_C<-c(N_C,n-a)}
  sam <- data.frame(N_E=N_E,N_C=N_C)
  sam
}



parameter<-function(n,eta,Seq){
  #bias vector
  PR <- matrix(0,nrow=nrow(Seq),ncol=n)
  for (i in 1:nrow(Seq)) {
    PR[i,] <- biasSeqfun(Seq[i,],eta)
  }
  
  #sample size
  sam<-samplesizes(n,Seq)
  n_E<-sam$N_E
  n_C<-sam$N_C
  
  #delta
  delta<-numeric(0)
  for (j in 1:nrow(Seq)){
    a<-10*mean(PR[j,][Seq[j,]==1]) - 10*mean(PR[j,][Seq[j,]==0])
    delta=c(delta,sqrt((n_E[j]*n_C[j])/(10*n))*a)}
  
  #lambda
  lambda<-numeric(0)
  for (j in 1:nrow(Seq)){
    a<-10*PR[j,]
    sq<-sum(a^2)
    lambda<-c(lambda,sq/10)
  }
  
  par<-data.frame(lambda=lambda,delta=delta)
  par
}




Error_two_sided<-function(n,eta,Seq,alpha){
  er_less <- numeric(0)
  er_high <- numeric(0)
  er <- numeric(0)
  par<-parameter(n,eta,Seq)
  for(j in 1:nrow(Seq)){
    p.value.less<-doublyT(qt(alpha/2,n-2), n-2, par$delta[j], par$lambda[j],ub=100)
    p.value.greater<-  1-doublyT(qt(1-alpha/2,n-2),n-2, par$delta[j], par$lambda[j],ub=100)
    p.value <- p.value.less+p.value.greater
    er=c(er,p.value)
    er_high=c(er_high,p.value.greater)
    er_less=c(er_less,p.value.less)}
  fr<-data.frame(er_less=er_less,er_high=er_high,er=er)
  fr
  
}






N<-list(10,20,50,100)

eta_10=list(0.02024,0.1012,0.2024)
eta_20=list(0.01325,0.06625,0.1325)
eta_50=list(0.00809,0.04045,0.0809)
eta_100=list(0.0056,0.028,0.056)
eta=list(eta_10,eta_20,eta_50,eta_100)

m<-10

mittlererFehler=matrix(0,4,3)
Proz_5=matrix(0,4,3)
Fehler<-numeric(0)



for (w in seq(1,4,1)){
  for( z in seq(1,3,1)){
    
    
    
    Rand<-rarPar(N[[w]],K=2,groups = LETTERS[1:2])
    myPar<- genSeq(Rand,10000)
    Rand_list<- getRandList(myPar)
    for (j in seq(1,dim(Rand_list)[1],1))
      for (i in seq(1,dim(Rand_list)[2],1))
        if ( Rand_list[j,i]=="A"){
          Rand_list[j,i]=1}else{
            Rand_list[j,i]=0}
    
    
    
    etas<-eta[[w]][[z]]
    
    alpha<-0.05
    
    errors<-Error_two_sided(N[[w]],etas,Rand_list,alpha)
    Fehler<-c(Fehler,errors$er)
    
    
    error_mittel=0
    j=0
    for (i in 1:length(errors$er)){
      if(errors$er[i]=="NaN"){
        error_mittel=error_mittel+0
        j=j+1}
      else{
        error_mittel=error_mittel+errors$er[i]}}
    
    
    error_mittel=error_mittel/(length(errors$er)-j)
    mittlererFehler[w,z]=error_mittel
  
    
    
    underfive=0
    notanumb=0
    for (i in 1:length(errors$er)){
      if(errors$er[i]=='NaN'){
        notanumb=notanumb+1}
      else if(round(errors$er[i],3)<=0.05){
        underfive=underfive+1}}
    
    
    proz=underfive/(length(errors$er)-notanumb)*100
    Proz_5[w,z]=proz
    
  }}

print(mittlererFehler)
print(Proz_5)
print(Fehler)

